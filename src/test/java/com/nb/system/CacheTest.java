﻿package com.nb.system;


import java.util.ArrayList;
import java.util.List;

import net.sf.ehcache.Cache;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.nb.common.utils.CacheUtils;
import com.nb.system.model.User;

/**
 * 测试类
 * @author mf
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring.xml", "classpath:spring-nutzdao.xml"  })
public class CacheTest {

	private static final Logger logger = Logger.getLogger(CacheTest.class);

	@Autowired
	private Dao dao;
	
	@Test
	public void getAll() {
		List<User>  list = dao.query(User.class, null);
		CacheUtils.put("cmsCache", "list", list);
		CacheUtils.put("cmsCache", "111", 1);
		CacheUtils.put("cmsCache", "22", 2);
		CacheUtils.put("cmsCache", "33", 3);
		CacheUtils.put("cmsCache", "4444", 3);
		List<User> list2 = (List<User>)CacheUtils.get("cmsCache", "list");
		if(list2==null){
			
		}
		Cache cache = CacheUtils.getCache("cmsCache");
		logger.info("===="+cache.getSize());
		logger.info("===="+cache.getMemoryStoreSize());
		logger.info("===="+cache.getStatistics().getCacheHits());
		logger.info("===="+cache.getStatistics().getCacheMisses());
		logger.info("===="+list2.size());
	}
	
	@Test
	public void getAll2() {
		for (int i = 0; i < 100; i++) {
			CacheUtils.put("cmsCache", "cache"+i, i+"");
		}
		Cache cache = CacheUtils.getCache("cmsCache");
		logger.info("===="+cache.getSize());
		logger.info("===="+cache.getMemoryStoreSize());
		logger.info("===="+cache.getStatistics().getCacheHits());
		logger.info("===="+cache.getStatistics().getCacheMisses());
		for (int i = 0; i < 100; i++) {
			logger.info("===="+CacheUtils.get("cmsCache", "cache"+i));
		}
		System.out.println(123);
	}
	
	@Test
	public void getAll3() {
		List list = new ArrayList();
		for (int i = 0; i < 100; i++) {
			list.add(i);
		}
		CacheUtils.put("cmsCache", "list", list);
		Cache cache = CacheUtils.getCache("cmsCache");
		logger.info("===="+cache.getSize());// 得到缓存中的对象数
		logger.info("===="+cache.getMemoryStoreSize());// 得到缓存对象占用内存的大小
		logger.info("===="+cache.getStatistics().getCacheHits());// 得到缓存读取的命中次数
		logger.info("===="+cache.getStatistics().getCacheMisses());// 得到缓存读取的错失次数
		list = (List)CacheUtils.get("cmsCache", "list");
		for (int i = 0; i < list.size(); i++) {
			logger.info("===="+list.get(i));
		}
	}
}
