<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>
<script>

$(function(){
	$.metadata.setType("attr", "validate");
    var v = $("form").validate({
        //调试状态，不会提交数据的
        debug: true,
        errorPlacement: function (lable, element) {

            if (element.hasClass("l-textarea")) {
                element.addClass("l-textarea-invalid");
            }
            else if (element.hasClass("l-text-field")) {
                element.parent().addClass("l-text-invalid");
            }

            var nextCell = element.parents("td:first").next("td");
            nextCell.find("div.l-exclamation").remove(); 
            $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
        },
        success: function (lable) {
            var element = $("#" + lable.attr("for"));
            var nextCell = element.parents("td:first").next("td");
            if (element.hasClass("l-textarea")) {
                element.removeClass("l-textarea-invalid");
            }
            else if (element.hasClass("l-text-field")) {
                element.parent().removeClass("l-text-invalid");
            }
            nextCell.find("div.l-exclamation").remove();
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("form").ligerForm();
});
</script>
<style>
body{ font-size:12px;}
.l-table-edit {}
.l-table-edit-td{ padding:4px;}
.l-button-submit,.l-button-test{width:80px; float:left; margin-left:10px; padding-bottom:2px;}
.l-verify-tip{ left:230px; top:120px;}
</style>
</head>
<body>
<form name="form1" method="post"  id="form1" action="${ctx}/system/role/save.do" >
<input type="hidden" name="id" value="${ob.id}" />
<table cellpadding="0" cellspacing="0" class="l-table-edit" >
<tr>
    <td align="right" class="l-table-edit-td">角色</td>
    <td align="left" class="l-table-edit-td" style="width:160px">
	<input name="rolename" value="${ob.rolename}" type="text" id="rolename" validate="{required:true}" />
    </td>
    <td align="left"></td>
</tr>
<tr>
    <td align="right" class="l-table-edit-td">描述</td>
    <td align="left" class="l-table-edit-td" style="width:160px">
	<input name="description" value="${ob.description}" type="text" id="description" />
    </td>
    <td align="left"></td>
</tr>
</table>
<p style="text-align: center">
<input type="submit" value="保存" id="Button1" class="l-button l-button-submit" /> 
</p>
</form>
</body>
</html>