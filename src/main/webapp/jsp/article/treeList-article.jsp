<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>
<script>

var setting = {
		view: {
			selectedMulti: false
		},
		edit:{
			enable: true,
			showRemoveBtn: false,
			showRenameBtn: false
		},
		check: {
			enable: false//是否显示checkbox
		},
		async: {
			enable: true,
			url:"${ctx}/admin/category/ajax_tree"
		},
		data: {
			key:{
				name:"name"
			},
			simpleData: {
				enable: true,//true时下面的设置生效
				idKey: "id",//id
				pIdKey: "pid",//pid
			}
		},
		callback: {
			onClick : onClick ,
			onAsyncSuccess: zTreeOnAsyncSuccess//异步加载成功
		}
	};
//异步加载成功后    展开所有节点
function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
	treeObj.expandAll(true);
};
function f_query(){
	ligergrid.loadData();
}
function onClick(event, treeId, treeNode) {
	f_query();
};
var treeObj = null;
$(function ()
{	
	treeObj = $.fn.zTree.init($("#menuTree"), setting);

	$("#backGrid").click(function(){
		   $(".rig").load("${ctx}/jsp/system/list-department.jsp");			
	});
	ligergrid =  $("#maingrid").ligerGrid({
        columns: 
			[ 
			 {display : 'ID',name : 'id',width :"5%"}, 
			 {display : '登录名',name : 'loginname'}, 
			 {display : '用户名',name : 'xm'}, 
			 {display : 'Email',name : 'mail',width :"20%"}, 
			 {display : '手机',name : 'phone'},
			 {
                 display: '操作', isAllowHide: false,
                 render: function (row)
                 {
                     var html ="";
                    html+='<button class="btn btn-mini btn-info" onclick="f_edit({0})" type="button">编辑</button>&nbsp;';
                     html+='<button class="btn btn-mini btn-danger" onclick="f_delete({0})" type="button">删除</button>';
                    return Free.replace(html,row.id);
                 }, width: 160
             }

        ],
        url: '${ctx}/system/user/ajax_list.do',
        pageSize: 10, sortName: 'id',
        width: '79%', height: 460, 
        checkbox : false,
        pageParmName:'page'
    });

	
});
</script>
<style type="text/css">
.ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
ul.ztree {
    background: none repeat scroll 0 0 #f0f6e4;
    border: 1px solid #d5d5d5;
    height: 459px;
    overflow-x: auto;
    overflow-y: auto;
    width: 150px;
}
</style>
</head>
<body style="background-color: #fff">
    <ul id="menuTree" class="ztree" style="float: left;overflow-x:hidden ;overflow-y:scroll;"></ul>

    <div id="maingrid" style="float: left;" ></div>
</div>
</body>	
</html>

