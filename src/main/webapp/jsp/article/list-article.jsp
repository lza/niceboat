<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>

<script type="text/javascript">

$(function (){
	ligergrid =  $("#maingrid").ligerGrid({
            columns: 
				[ 
					{display : '文章标题',name : 'title', width: "30%"}, 
					{display : '优先级',name : 'sx'}, 
            	<c:if test="${category.showMode==3}">
					{display : '图片',name : 'imagesrc', width: 400}, 
            	</c:if>
					{display : '创建时间' , width: 160,render: function (row)
	                     {
						
							var html ="";//日期对象
	                        return new Date(row.createtime).Format("yyyy-MM-dd hh:mm:ss");
	                     }
	                 },
				    {
                     display: '操作', isAllowHide: false,
                     render: function (row)
                     {
                         var html ="";
                         html+='<a href="javascript:return void(0);" onclick="f_edit({0})"  >编辑</a>&nbsp;';
                         html+='<a href="javascript:return void(0);"  onclick="f_delete({0})" >删除</a>';
                         return Free.replace(html,row.id);
                     }, width: 160
                 }

            ],
            url: '${ctx}/admin/article/ajax_list?search_EQ_categoryid=${param.categoryid}',
            pageSize: 20, sortName: 'id',
            width: '97%', height: '96%', 
            checkbox : false,
            pageParmName:'page',
            enabledSort:false
        });
        
    });
function f_query(){
	ligergrid.set('parms',$('#query-form').serializeArray());
	ligergrid.loadData();
}
function f_edit(id){
  parent.newDialog({type:'iframe',value:'${ctx}/admin/article/input?id='+id},
		{title:"文件编辑",modal:true,width:1000,height:580});
}

function f_add(){
	parent.newDialog({type:'iframe',value:'${ctx}/admin/article/input?categoryid=${param.categoryid}'},
		{title:"文章新增",modal:true,width:1000,height:580});
}
function f_delete(id){
	  freeConfirm("是否将此信息删除?",function(){
		  Free.ajax({
	         	   url: '${ctx}/admin/article/delete.do',
	         	   data: {id:id},
	         	   success: function(data){
	         		   if (data='ok'){
	         			 ligergrid.loadData();
	         		   }
		         	}
			  });
		});
}
</script>
</head>
<body style="background-color: #fff">
        <table class="tab" >
          <tbody>
          <tr class="tab_white02">
            <td>     
			<form class="form-horizontal" id="query-form" method="get" action="stat-system" style="width:400px">
					<i class="icon-search"></i>文章标题：
						<input type="text" placeholder="模糊查询..." name="search_LIKE_title">
					<button type="button" onclick="f_query();" class="l-button"><i class="icon-search  icon-white"></i> 查询</button>
					<button type="button" onclick="f_add();"  class="l-button"><i class="icon-plus icon-white"></i> 新增</button>
					
			</form>
            </td>
          </tr>
        </tbody></table>
        <div id="maingrid" ></div>
</body>	

</html>
