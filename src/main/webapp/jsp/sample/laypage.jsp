<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<script src="${ctx}/ui/plugins/jquery/jquery-1.10.2.min.js" type="text/javascript"></script> 
<script src="${ctx}/ui/plugins/laypage/laypage.js" type="text/javascript"></script> 

<script type="text/javascript">
$(function (){
	demo();
 });
 
function demo(curr) {
    var pageSize = 10;

    //以下将以jquery.ajax为例，演示一个异步分页
    $.getJSON('${ctx}/system/user/ajax_list.do', {
        page: curr || 1,
        pageSize: pageSize
    },
    function (res) { //从第1页开始请求。返回的json格式可以任意定义
        laypage({
            cont: 'page1', //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
            pages: Math.ceil(res.Total/pageSize), //通过后台拿到的总页数
            curr: curr || 1,
            //first: '首页', //若不显示，设置false即可
            //last: '尾页', //若不显示，设置false即可
            //prev: '<', //若不显示，设置false即可
            //next: '>', //若不显示，设置false即可
            jump: function (obj,first) { //触发分页后的回调
            	 if(!first){ //点击跳页触发函数自身，并传递当前页：obj.curr
                     demo(obj.curr);
                 }
            }
        });
      	 $('#tbody').html(PackagData(res));
    });
}
function PackagData(res){
var content="";
   	$.each(res.Rows,function(i,o){
   		content+="<tr><td>";
   		content+=o.id;
   		content+="</td><td>";
   		content+=o.loginname;
   		content+="</td><td>";
   		content+=o.xm;
   		content+="</td></tr>";
   		
   	});
   	return content;
}
</script>
</head>
<body>
这是一个laypage的分页组件，配合ajax使用还是不错的，快速就做出了一个grid<br/><br/>
<table id="Result" cellspacing="0" cellpadding="0" border="1">
            <tr>
                <th width="50" >id</th>
                <th width="100" >登录名</th>
                <th width="100" >姓名</th>
            </tr>
            <tbody id="tbody">
            </tbody>
        </table>
	<div id="page1"></div>
</body>	
</html>
