<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<script src="${ctx}/ui/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
<script src="${ctx}/ui/plugins/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
<script src="${ctx}/ui/plugins/jquery-validation/messages_cn.js" type="text/javascript"></script>
<script src="${ctx}/ui/plugins/layer/layer.min.js" type="text/javascript"></script>
<script>

$(function(){

    $.metadata.setType("attr", "validate");
    var v = $("#inputForm").validate({
         //调试状态，不会提交数据的
         debug: true,
         errorPlacement: function (lable, element) {

             if (element.hasClass("l-textarea")) {
                 element.addClass("l-textarea-invalid");
             }
             else if (element.hasClass("l-text-field")) {
                 element.parent().addClass("l-text-invalid");
             }

             var nextCell = element.parents("td:first").next("td");
             nextCell.find("div.l-exclamation").remove(); 
             $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
         },
         success: function (lable) {
             var element = $("#" + lable.attr("for"));
             var nextCell = element.parents("td:first").next("td");
             if (element.hasClass("l-textarea")) {
                 element.removeClass("l-textarea-invalid");
             }
             else if (element.hasClass("l-text-field")) {
                 element.parent().removeClass("l-text-invalid");
             }
             nextCell.find("div.l-exclamation").remove();
         },
         submitHandler: function (form) {
           form.submit();
         }
     });
    $("#inputForm").ligerForm({inputWidth:470});
    
});
</script>
</head>
<body style="background-color: #fff">

	<form id="inputForm" method="post" action="${ctx}/admin/site/save" class="form-horizontal">
	<input type="hidden" name="id" value="${ob.id }" >
		<table cellpadding="0" cellspacing="0" class="l-table-edit"  >
            <tr>
                <td align="right"  width="100px" >站点名称:</td>
                <td align="left"  width="464px"  >
					<input type="text" class="form-control" id="name" name="name" value="${ob.name }" validate="{required:true}"  />
                </td>
                <td align="left" width="100px" ></td>
            </tr>
            <tr>
                <td align="right" >站点标题:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="title" value="${ob.title }"/>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >站点logo:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="logo" value="${ob.logo }" />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >描述:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="description" value="${ob.description }"  />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >关键字:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="keywords" value="${ob.keywords }"  />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >主题:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="theme" value="${ob.theme }"  />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >版权信息:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="copyright" value="${ob.copyright }"  />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" ></td>
                <td align="left"  >
					<input type="submit" value="保存" id="Button1" class="l-button l-button-submit" />
                </td>
                <td align="left"></td>
            </tr>
        </table> 
	</form>
</body>	

</html>