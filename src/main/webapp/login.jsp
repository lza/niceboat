<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>NiceBoat CMS 后台管理系统</title>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<link rel="stylesheet" type="text/css" href="${ctx }/ui/css/login.css"/>
<script src="${ctx }/ui/plugins/jquery/jquery-1.10.2.min.js"></script>
<link rel="shortcut icon" href="" />

<script type="text/javascript">
function CheckForm()
{
	if ($("#loginname").val()=="" || $("#password").val()==""){
		alert("用户名和密码不能为空");
		return false;
	}
   return true;
}

</script>
</head>
<body onload="javascript:document.form1.PASSWORD.focus();" scroll="auto">

<form name="form1" method="post" action="${ctx}/system/user/login" autocomplete="off" onsubmit="return CheckForm();">
<div id="logo">
   <div id="form">
      <div class="left">
         <div class="user">
        <input type="text" class="text" id="loginname" name="loginname" maxlength="20" onmouseover="this.focus()" onfocus="this.select()" value=""></div>
         <div class="pwd">
           <input type="password" id="password" class="text" name="password" onmouseover="this.focus()" onfocus="this.select()" value="" />
        </div>
		       <div class="right">
         <input type="submit" class="submit" title="登录" value="" />
      </div>
      </div>
   </div>
   </div>
</div>

</form>
<div align="center" class="msg">
   <div></div>
   <div></div>
   <div></div>
   <div>
</div>
</div>
<div align="center" class="msg">
	<div><span style='color:red;margin-right:10px;'>用户初始密码：1</span></div>
</div>

</body>
</html>