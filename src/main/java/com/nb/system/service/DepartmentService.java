package com.nb.system.service;

import java.util.List;

import org.nutz.dao.Condition;
import org.nutz.dao.Dao;
import org.nutz.dao.QueryResult;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.common.service.BaseService;
import com.nb.system.NewPager;
import com.nb.system.model.Department;

/**
 * 工作部门
 * 模板自动生成   for FreeUI
 * @author mefly
 *
 */

@Service
public class DepartmentService extends BaseService {
	
	@Autowired
	private Dao dao;

	//删除
	public int delete(Integer id) {
		return dao.delete(Department.class,id);
	}

	//插入
	public Department insert(Department record) {
		return dao.insert(record);
	}

	//取单条
	public Department fetch(Integer id) {
		return dao.fetch(Department.class,id);
	}

	//更新非空
	public int updateIgnoreNull(Department record) {
		return dao.updateIgnoreNull(record);
	}

	//更新
	public int update(Department record) {
		return dao.update(record);
	}

	//查询
	public List<Department> query(Condition c){
		return dao.query(Department.class,c, null);
	}
	
	//分页查询
	public QueryResult queryPage(NewPager page){
		Criteria cri = getCriteriaFromPage(page);
		
	    List<Department> list = dao.query(Department.class, cri, page);
	    page.setRecordCount(dao.count(Department.class, cri));
	    return new QueryResult(list, page);
	}
	
}
